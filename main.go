package main

import (
	"fmt"
	"math"
	"time"
)

// Max possible result, all 10-digits palindromes % 11 == 0
const max = 999999999

type result struct {
	palindrome int
	left       int
	right      int
}

func main() {
	startTime := time.Now().UnixNano()
	result := findResult()
	endTime := time.Now().UnixNano()

	fmt.Printf("Palindrome: %d\n", result.palindrome)
	fmt.Printf("Prime numbers: %d * %d\n", result.left, result.right)
	fmt.Printf("Duration: %d\n", endTime-startTime)
}

func findResult() result {
	numbers := getPrimeNumbers(10000, 99999)

	size := len(numbers)
	current := result{}

	for i := 0; i < size; i++ {
		left := numbers[i]

		for j := i + 1; j < size; j++ {
			right := numbers[j]
			possible := left * right

			if possible > max {
				break
			}

			if possible > current.palindrome && isPalindrome(possible) {
				current = result{possible, left, right}
			}
		}
	}

	return current
}

func getPrimeNumbers(from, to int) []int {
	size := to + 1

	variants := make([]bool, size)

	limit := int(math.Ceil(math.Sqrt(float64(to))))

	for i := 2; i < limit; i++ {
		if variants[i] == false {
			for j := i * i; j < size; j += i {
				variants[j] = true
			}
		}
	}

	result := make([]int, 0)
	for i := from; i < size; i++ {
		if variants[i] == false {
			result = append(result, i)
		}
	}

	return result
}

func isPalindrome(value int) bool {
	const (
		max   = 9
		limit = max >> 1
		right = max - 1
	)

	var (
		digits = [max]int{}
	)

	i := 0
	for value > 0 {
		digits[i] = value % 10
		i++

		value /= 10
	}

	for j := 0; j < limit; j++ {
		if digits[j] != digits[right-j] {
			return false
		}
	}

	return true
}
